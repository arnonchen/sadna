#include "Integer.h"
Integer::Integer(int val)
{
	this->setIsTemp(true);
	_value = val;
}
const bool Integer::isPrintable()
{
	return true;
}
std::string Integer::toString() const
{
	return  std::to_string(_value);
}