#include "String.h"
String::String(std::string str)
{
	_value = str;
	this->setIsTemp(true);
}
const bool String::isPrintable()
{
	return true;
}
std::string String::toString() const
{
	return std::string(_value);
}