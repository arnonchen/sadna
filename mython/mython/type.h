#ifndef TYPE_H
#define TYPE_H
#include <iostream>
class Type
{
	private:
		bool _isTemp = false;
	public:
		void setIsTemp(bool val);
		bool getIsTemp();
		virtual std::string toString() const = 0;
};





#endif //TYPE_H
