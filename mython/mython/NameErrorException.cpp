#include "NameErrorException.h"

const char* NameErrorException::what() const throw()
{
	std::string re = "NameError: name [" + _name + "] is not defined";
	char* a;
	strcpy(a , re.c_str());
	return a;
}

void NameErrorException::setName(std::string name)
{
	_name = name;
}
std::string NameErrorException::getName()
{
	return _name;
}