#ifndef VOID_H
#define VOID_H
#include "type.h"
#include <iostream>
#include <string>
class Void : public Type
{
public:
	Void();
	const bool isPrintable();
	const std::string toString();
private:

};


#endif // VOID_H