#ifndef STRING_H
#define STRING_H

#include "Sequence.h"
#include <iostream>
#include <string>

class String : public Sequence
{
public:
	String(std::string str);
	const bool isPrintable();
	virtual std::string toString() const;
private:
	std::string _value;
};

#endif // STRING_H