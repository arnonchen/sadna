#ifndef BOOLEAN_H
#define BOOLEAN_H
#include "type.h"
#include <iostream>
#include <string>
class Boolean : public Type
{
public:
	Boolean(bool val);
	const bool isPrintable();
	virtual std::string toString() const;
private:
	bool _value;
};


#endif // BOOLEAN_H