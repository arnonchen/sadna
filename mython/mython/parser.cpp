#include "parser.h"
#include <iostream>
#include "IndentationException.h"
#include "Integer.h"
#include "Boolean.h"
#include "String.h"
#include "Helper.h"
Type* Parser::parseString(std::string str) 
{
	if (getType(str) != NULL)
	{
		return getType(str);
	}
	if (str.back() == 32 || str.back() == 9)
	{
		throw IndentationException();
	}
	else
		return NULL;
}

Type* Parser::getType(std::string& str)
{
	if (Helper::isInteger(str))
	{
		Integer *i = new Integer(std::stoi(str));
		return i;
	}
	else if (Helper::isBoolean(str))
	{
		if (str == "False")
		{
			Boolean* i = new Boolean(false);
			return i;
		}
		else
		{
			Boolean* i = new Boolean(true);
			return i;
		}
	}
	else if (Helper::isString(str))
	{
		String* i = new String(str);
		return i;
	}
	else
		return NULL;
}

bool Parser::isLegalVarName(const std::string& str)
{
	if (Helper::isDigit(str[0]))
	{
		return false;
	}
	for (int i = 0; i < str.size(); i++)
	{
		if (!(Helper::isLetter(str[i])) || str[i] != '_')
		{
			return false;
		}
	}
	return true;
}

bool Parser::makeAssignment(const std::string& str)
{

}