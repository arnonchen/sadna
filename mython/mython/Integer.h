#ifndef INTEGER_H
#define INTEGER_H
#include "type.h"
#include <iostream>
#include <string>
class Integer : public Type
{
	public:
		Integer(int val);
		const bool isPrintable();
		virtual std::string toString() const;
	private:
		int _value;
};

#endif // INTEGER_H