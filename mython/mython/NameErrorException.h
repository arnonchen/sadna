#ifndef NAME_ERROR_EXCEPTION_H
#define NAME_ERROR_EXCEPTION_H
#include <iostream>
#include "InterperterException.h"

class NameErrorException : public InterperterException
{
public:
	virtual const char* what() const throw();
	void setName(std::string name);
	std::string getName();
private:
	std::string _name;
};

#endif // NAME_ERROR_EXCEPTION_H