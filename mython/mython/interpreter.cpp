#include "type.h"
#include "IndentationException.h"
#include "InterperterException.h"
#include "parser.h"
#include <iostream>
#include "SyntaxException.h"
#define WELCOME "Welcome to Magshimim Python Interperter version 1.0 by "
#define YOUR_NAME "[ARNON CHEN]"


int main(int argc,char **argv)
{
	std::cout << WELCOME << YOUR_NAME << std::endl;

	std::string input_string;

	// get new command from user
	std::cout << ">>> ";
	std::getline(std::cin, input_string);
	
	while (input_string != "quit()")
	{
		// prasing command
		try
		{
			Type* a = Parser::parseString(input_string);
			try
			{
				if (a == NULL)
				{
					throw SyntaxException();
				}
				else
				{
					std::cout << a->toString() << "\n";
				}
			}
			catch (SyntaxException& e)
			{
				std::cout << e.what() << "\n";
			}
		}
		catch (IndentationException& e)
		{
			std::cout << e.what() << "\n";
		}
		// get new command from user
		std::cout << ">>> ";
		std::getline(std::cin, input_string);
	}

	return 0;
}


