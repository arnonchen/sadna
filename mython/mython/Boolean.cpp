#include "Boolean.h"
Boolean::Boolean(bool val)
{
	this->setIsTemp(true);
	_value = val;
}
const bool Boolean::isPrintable()
{
	return true;
}
std::string Boolean::toString() const
{
	if (_value)
		return "True";
	else
		return "False";
}